#!/bin/bash
echo -e "Installation de neovim \n"
sudo pacman -Syy neovim --noconfirm
cp init.vim $home/.config/nvim/
cp -r colors $home/.config/
### Installation de Plug Vim ###
echo -e "Installation de Plug Vim \n"
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
  468         https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
mkdir $home/.config/nvim/autoload && cp plug.vim $home/.config/nvim/autoload/
